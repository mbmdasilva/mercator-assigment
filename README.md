# mercator-assigment

Assignment for Mercator consultancy Automation Test  position

## Getting started

In order to run this tests successfully please follow the steps
- Make sure you have chromedriver added in the computer to run the tests (https://chromedriver.chromium.org/getting-started)
- Alternatively use the RunCukesTest class to run the tests on an IDE
- Run mvn dependency:resolve
- Run mvn compile
- Run mvn clean verify this will run the tests




