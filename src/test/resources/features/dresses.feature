@test
Feature:Find most expensive dress and add to checkout

  Scenario: Most expensive dress is found and added to the cart

    Given I am a in the shopping webpage
    When I click the dress tab
    And  I select the most expensive dress
    And I add the dress to the basket
    Then the product is added in the cart with notification