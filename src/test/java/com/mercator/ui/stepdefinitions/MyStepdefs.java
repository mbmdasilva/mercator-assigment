package com.mercator.ui.stepdefinitions;

import com.mercator.ui.pages.DressesPage;
import com.mercator.ui.support.Driver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertTrue;

public class MyStepdefs {

    DressesPage dressesPage = new DressesPage();
    @Given("I am a in the shopping webpage")
    public void iAmAInTheShoppingWebpage() {
        Driver.navigateToPage();
    }

    @When("I click the dress tab")
    public void iClickTheDressTab() {
        dressesPage.clickOnDressesMenuItem();
    }

    @And("I select the most expensive dress")
    public void iSelectTheMostExpensiveDress() {
        dressesPage.clickEveningDresses();
        dressesPage.selectMostExpensiveDress();
    }

    @And("I add the dress to the basket")
    public void iAddTheDressToTheBasket() {
        dressesPage.addDressToBasket();
    }

    @Then("the product is added in the cart with notification")
    public void theProductIsAddedInTheCartWithNotification() {
        assertTrue("Notification is not Enabled",dressesPage.isNotificationEnabled());
    }
}

