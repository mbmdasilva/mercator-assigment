package com.mercator.ui.pages;


import com.mercator.ui.support.Driver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class DressesPage extends Driver {
   // WebDriverWait wait = new WebDriverWait(driver,10);

    @FindBy(css = "#block_top_menu > ul > li:nth-child(2) > a")
    private WebElement dressesMenu;

    @FindBy(linkText = "Evening Dresses")
    private WebElement eveningDresses;

    @FindBy(css = ".product_img_link > .replace-2x")
    private WebElement expensiveDress;

    @FindBy(css ="a.button.lnk_view.btn.btn-default > span")
    private WebElement preview;

    @FindBy(css = "#add_to_cart > button")
    private WebElement addToCart;

    @FindBy(css = "h2:nth-child(2)")
    private WebElement notificationElement;


    public DressesPage() {
        PageFactory.initElements(driver, this);
    }

    public void clickOnDressesMenuItem(){
        dressesMenu.click();
    }

    public void clickEveningDresses(){
        JavascriptExecutor je =  (JavascriptExecutor) driver;
        je.executeScript("window.scrollBy(0,250)", "");
        eveningDresses.click();
    }
    public void selectMostExpensiveDress(){
        JavascriptExecutor je =  (JavascriptExecutor) driver;
        je.executeScript("var mouseEvent = document.createEvent('MouseEvents');mouseEvent.initEvent('mouseover', true, true); arguments[0].dispatchEvent(mouseEvent);", expensiveDress);
        preview.click();
    }
    public void addDressToBasket(){
        addToCart.click();
    }
    public boolean isNotificationEnabled(){
     return notificationElement.isEnabled();
    }

}
