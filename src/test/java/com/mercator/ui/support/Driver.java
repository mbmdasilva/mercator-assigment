package com.mercator.ui.support;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class Driver {

    public static WebDriver driver;

    public static void navigateToPage() {
        driver.get("http://automationpractice.com/index.php");
        driver.manage().window().setSize(new Dimension(1493, 1255));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
    }

    public static void initialiseDriver() {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
    }

    public static void quitDriver() {
        driver.quit();
    }

}



