package com.mercator.ui.cucumber;

import com.mercator.ui.support.Driver;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true,
        tags = "@test",
        features = "src/test/resources/features",
        glue = {"com.mercator.ui.cucumber",
                "com.mercator.ui.stepdefinitions",
                "com.mercator.ui.support",
                "com.mercator.ui.pages"})

public class RunCukesTest {
    @BeforeClass
    public static void setup() {
        Driver.initialiseDriver();
    }


    @AfterClass

    public static void teardown() {
        Driver.quitDriver();
    }

}
